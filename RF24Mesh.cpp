#include "arduino.h"
#include "../RF24/RF24.h"
#include "RF24MeshCfg.h"
#include "RF24Mesh.h"



RF24Mesh::RF24Mesh(RF24* _radio, byte _node, byte _uplink_node, byte *_routing_table)
	:radio(*_radio), node(_node), uplink_node(_uplink_node), routing_table(_routing_table)
{
	
}

void RF24Mesh::begin()
{
    // Set up the radio the way we want it to look
    radio.setChannel(RF_CHANNEL);
    radio.setDataRate(RF_SPEED);
    radio.setCRCLength(RF24_CRC_16);

    // We will be using the Ack Payload feature, so please enable it
    radio.enableAckPayload();

}

void RF24Mesh::poll()
{
	
}

bool RF24Mesh::available()
{
	
}

void RF24Mesh::read(rf_header *)
{
	
}

void RF24Mesh::send(rf_header *)
{
	
}