#ifndef __RF24_MESH_H__
#define __RF24_MESH_H__

// Maximum nordic packet size id 32 byte
#define MAX_PACKET_SIZE 32

// Buffer for keeping unprocessed packets
// #define MAX_QUEUE_SIZE 3

typedef unsigned char byte;
class RF24;

typedef struct
{
	byte from;
	byte to;
	unsigned int type: 2;
	unsigned int seq: 14;
} rf_header;

typedef struct
{
	rf_header header;
	byte data[MAX_PACKET_SIZE-sizeof(rf_header)];
	byte data_size;
} rf_packet;

typedef enum {uplink, my, downlink, broadcast, bootloader} pipe_type;

class RF24Mesh
{
	byte *routing_table;
	RF24& radio;
	byte  node, uplink_node;
	
	uint64_t pipe_address(pipe_type pt);
	
public:
	RF24Mesh(RF24* _radio, byte node, byte uplink_node, byte *routing_table=NULL);
	
	void begin();
	void poll();
	bool available();
	void read(rf_header *);
	void send(rf_header *);
};

#endif // __RF24_MESH_H__
