#ifndef __RF24_MESH_CFG_H__
#define __RF24_MESH_CFG_H__

#define RF_CHANNEL 80
#define RF_SPEED RF24_250KBPS

#define RF_BASE_PIPE 0x3A5A3A5A00LL

#endif // __RF24_MESH_CFG_H__
